<div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul class="mobile-menu-nav">
                                        <!-- <li><a data-toggle="collapse" data-target="#Charts" href="analytics.php">Dashboard <span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                        </li> -->
                                        <li><a data-toggle="collapse" data-target="#demoevent" href="#">Provider <span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul id="demoevent" class="collapse dropdown-header-top">
                                                <li><a href="all-provider.php">All Provider</a>
                                                </li>
                                                <li><a href="request-provider.php">Request Provider</a>
                                                </li>
                                                <!-- <li><a href="add-provider.php">Add Provider</a>
                                                </li>
                                                <li><a href="edit-provider.php">Edit Provider</a>
                                                </li>
                                                <li><a href="provider-profile.php">Provider Profile</a>
                                                </li> -->
                                            </ul>
                                        </li>
                                        <!-- <li><a data-toggle="collapse" data-target="#demopro" href="#">User <span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul id="demopro" class="collapse dropdown-header-top">
                                                <li><a href="all-user.php">All User</a>
                                                </li> -->
                                                <!-- <li><a href="add-user.php">Add User</a>
                                                </li>
                                                <li><a href="edit-user.php">Edit User</a>
                                                </li>
                                                <li><a href="user-profile.php">User Profile</a>
                                                </li> -->
                                            <!-- </ul>
                                        </li> -->
                                        <li><a data-toggle="collapse" data-target="#demodepart" href="#">Category <span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul id="demodepart" class="collapse dropdown-header-top">
                                                <li><a href="category.php">Category List</a>
                                                </li>
                                                <li><a href="add-category.php">Add Category</a>
                                                </li>
                                                <li><a href="edit-category.php">Edit Category</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a data-toggle="collapse" data-target="#Tablesmob" href="#">Order <span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul id="Tablesmob" class="collapse dropdown-header-top">
                                                
                                                <li><a href="order.php">Order List</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                        <a data-toggle="collapse" data-target="#Tablesmob" href="#">Bank<span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul id="Tablesmob1" class="collapse dropdown-header-top">
                                                <!-- <li><a title="Peity Charts" href="static-table.php"><span class="mini-sub-pro">Static Table</span></a></li> -->
                                                <!-- <li><a title="Data Table" href="bank.php"><span class="mini-sub-pro">Bank Details</span></a></li> -->
                                                <li><a href="bank.php">Bank Details</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>