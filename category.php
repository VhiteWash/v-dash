<?php
 include("conn.php");
  $stmt = $conn->prepare("SELECT c.id,c.name,c.status,c.img,sc.img,sc.name FROM category c LEFT JOIN subcategory sc on c.id = sc.category_id");
  $stmt->store_result();
  $stmt->bind_result($id,$name,$status,$img,$subimg,$scname);
  $stmt->execute();
//  $sql = "SELECT c.id,c.name,c.status,sc.name FORM category c JOIN subcategory sc where c.id = sc.category_id ";
?>
<!doctype html>
<html class="no-js" lang="en">

<?php include("header.php")?>

<body>
    <?php include("sidebar.php")?>
    <div class="all-content-wrapper">
        <div class="header-advance-area">
            <div class="header-top-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header-top-wraper">
                                <div class="row">
                                    <div class="col-lg-1 col-md-0 col-sm-1 col-xs-12">
                                        <div class="menu-switcher-pro">
                                            <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
													<i class="educate-icon educate-nav"></i>
												</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div></br></br></br>
            <?php include("menubar.php")?>
            <div class="breadcome-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="breadcome-list single-page-breadcome">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="breadcome-heading">
                                            <form role="search" class="sr-input-func">
                                                <input type="text" placeholder="Search..." class="search-int form-control">
                                                <a href="#"><i class="fa fa-search"></i></a>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <ul class="breadcome-menu">
                                            <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                            </li>
                                            <li><span class="bread-blod">Category</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-status mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-status-wrap drp-lst">
                            <h4>Category List</h4>
                            <div class="add-product">
                                <a href="add-category.php">Add Category</a>
                            </div>
                            <div class="asset-inner">
                                <table>
                                    <tr>
                                        <th>No</th>
                                        <th>Name of Cate.</th>
                                        <th>Name of SubCate.</th>
                                        <th>Status</th>
                                        <th>Image</th>
                                        
                                    </tr>
                                    <?php
                                    $x=1;
                                          while($stmt->fetch())
                                          {
                                            ?>
                                    <tr>
                                        <td><?php echo $x; ?></td>
                                        <td><?php echo $name; ?></td>
                                        <td><?php echo $scname; ?></td>
                                        <td><?php echo $status; ?></td>
                                        <?php if(!empty($subimg)){ ?>
                                        <td><img src="<?php echo $subimg; ?>"/></td>
                                        <?php } else{ ?>
                                        <td><img src="<?php echo $img; ?>"/></td>
                                        <?php }?>
                                    </tr>
                                    <?php $x++; } ?>
                                </table>
                            </div>
                            <div class="custom-pagination">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">
                                        <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("footer.php")?>
    </div>
    <?php include("footerjs.php")?>
    
   
    
</body>

</html>