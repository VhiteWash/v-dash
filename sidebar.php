<div class="left-sidebar-pro">
        <nav id="sidebar" class="">
            <div class="sidebar-header">
                <a href="all-provider.php"><img class="img" src="img/logo/logosn.png" alt="" /></a>
                <strong><a href="all-provider.php"><img class="img1"src="img/logo/logo.png" alt="" /></a></strong>
            </div>
            <div class="left-custom-menu-adp-wrap comment-scrollbar">
                <nav class="sidebar-nav left-sidebar-menu-pro">
                    <ul class="metismenu" id="menu1">
                        <!-- <li class="active">
                            <a   href="analytics.php">
                                <span class="educate-icon educate-home icon-wrap"></span>
                                <span class="mini-click-non">Dashboard</span>
							</a>
                        </li> -->
                        <li>
                            <a class="has-arrow" href="all-provider.php" aria-expanded="false"><span class="educate-icon educate-professor icon-wrap"></span> <span class="mini-click-non">Provider</span></a>
                            <ul class="submenu-angle" aria-expanded="false">
                                <li><a title="All Professors" href="all-provider.php"><span class="mini-sub-pro">All Provider</span></a></li>
                                <li><a title="Add Professor" href="request-provider.php"><span class="mini-sub-pro">Request Provider</span></a></li>
                                <!-- <li><a title="Add Professor" href="add-provider.php"><span class="mini-sub-pro">Add Provider</span></a></li>
                                <li><a title="Edit Professor" href="edit-provider.php"><span class="mini-sub-pro">Edit Provider</span></a></li> -->
                                <!-- <li><a title="Professor Profile" href="provider-profile.php"><span class="mini-sub-pro">Provider Profile</span></a></li> -->
                            </ul>
                        </li>
                        <!-- <li>
                            <a class="has-arrow" href="all-user.php" aria-expanded="false"><span class="educate-icon educate-student icon-wrap"></span> <span class="mini-click-non">Users</span></a>
                            <ul class="submenu-angle" aria-expanded="false">
                                <li><a title="All Students" href="all-user.php"><span class="mini-sub-pro">All Users</span></a></li> -->
                                <!-- <li><a title="Add Students" href="add-user.php"><span class="mini-sub-pro">Add Users</span></a></li>
                                <li><a title="Edit Students" href="edit-user.php"><span class="mini-sub-pro">Edit Users</span></a></li>
                                <li><a title="Students Profile" href="user-profile.php"><span class="mini-sub-pro">Users Profile</span></a></li> -->
                            <!-- </ul>
                        </li> -->
                        <li>
                            <a class="has-arrow" href="category.php" aria-expanded="false"><span class="educate-icon educate-department icon-wrap"></span> <span class="mini-click-non">Category</span></a>
                            <ul class="submenu-angle" aria-expanded="false">
                                <li><a title="Departments List" href="category.php"><span class="mini-sub-pro">Category List</span></a></li>
                                <li><a title="Add Departments" href="add-category.php"><span class="mini-sub-pro">Add Category</span></a></li>
                                <li><a title="Edit Departments" href="edit-category.php"><span class="mini-sub-pro">Edit Category</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow" href="order.php" aria-expanded="false"><span class="educate-icon educate-data-table icon-wrap"></span> <span class="mini-click-non">Order</span></a>
                            <ul class="submenu-angle" aria-expanded="false">
                                <!-- <li><a title="Peity Charts" href="static-table.php"><span class="mini-sub-pro">Static Table</span></a></li> -->
                                <li><a title="Data Table" href="order.php"><span class="mini-sub-pro">Order List</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow" href="bank.php" aria-expanded="false"><span class="educate-icon educate-data-table icon-wrap"></span> <span class="mini-click-non">Bank</span></a>
                            <ul class="submenu-angle" aria-expanded="false">
                                <!-- <li><a title="Peity Charts" href="static-table.php"><span class="mini-sub-pro">Static Table</span></a></li> -->
                                <li><a title="Data Table" href="bank.php"><span class="mini-sub-pro">Bank Details</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </nav>
    </div>