<?php 
include("conn.php");
$status='pending';
$stmt = $conn->prepare("SELECT id,name,SUBSTRING(mobile,3,12) as mobile from agent WHERE status = ?");
$stmt->bind_param('s',$status);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($id,$name,$mobile);


?>
<!doctype html>
<html class="no-js" lang="en">

<?php include("header.php")?>
<body>
    <!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
    <!-- Start Left menu area -->
    <?php include("sidebar.php")?>
    <!-- End Left menu area -->
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="header-advance-area">
            <div class="header-top-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header-top-wraper">
                                <div class="row">
                                    <div class="col-lg-1 col-md-0 col-sm-1 col-xs-12">
                                        <div class="menu-switcher-pro">
                                            <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
													<i class="educate-icon educate-nav"></i>
												</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu start -->
            <?php include("menubar.php")?></br></br></br> 
            <!-- Mobile Menu end -->
            <div class="breadcome-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="breadcome-list">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="breadcome-heading">
                                            <form role="search" class="sr-input-func">
                                                <input type="text" placeholder="Search..." class="search-int form-control">
                                                <a href="#"><i class="fa fa-search"></i></a>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <ul class="breadcome-menu">
                                            <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                            </li>
                                            <li><span class="bread-blod">All Providere</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contacts-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <?php
                        while($stmt->fetch()){
                    ?>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="hpanel hblue contact-panel contact-panel-cs mg-b-30">
                            <div class="panel-body custom-panel-jw center">
                                <!-- <img alt="logo" class="img-circle m-b" src="img/contact/1.jpg"> -->
                                <h3><a href=""><?php echo $name; ?></a></h3>
                                <p>
                                <?php echo $mobile; ?>
                                </p>
                            </div>
                            <div class=" ">
                                <div class="professor-stds-int">
                                    
                                        <a  href="provider-profile.php?id=<?php echo $id; ?>&status=pending" class="btn-success btn" >View Info</a>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                        <?php }?>
                </div>
            </div>
        </div>
        <?php include("footer.php")?>
    </div>
    <?php include("footerjs.php")?>
    
</body>

</html>