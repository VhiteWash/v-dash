<?php
    include("conn.php");
    $stmt = $conn->prepare("SELECT id,name FROM agent WHERE status='ACTIVE'");
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($id,$agent_name);
    $stmt->fetch();

if(isset($_POST['Submit']))
    {
        include("conn.php");
        $a = $_POST['agent'];
        $c = $_POST['account'];
        $d = $_POST['ifsc'];
        $stmt1 = $conn->prepare("SELECT id FROM bank WHERE agent_id = '$a'");
        $stmt1->execute();
        $stmt1->store_result();
        $stmt1->bind_result($id);
        $stmt1->fetch();
       
        if($stmt1->affected_rows == 1)
        {
            $update = "UPDATE agent_bank_detail  SET acno = '$c',ifsc = '$d'  WHERE agent_id = '$a'";
            if ($conn->query($update) === TRUE) {
                header("Location: all-provider.php");
            } else {
                echo "Error updating record: " . $conn->error;
            }
        }
        else
        {
            $insert = "INSERT INTO agent_bank_detail (agent_id, acno, ifsc) VALUES('$a', '$c', '$d')";
            if ($conn->query($insert) === TRUE) {
                header("Location: all-provider.php");
            } else {
                echo "Error updating record: " . $conn->error;
            }
        }
        
    }
?>


<!doctype html>
<html class="no-js" lang="en">
<style>
    .drop{
        width: 100%;
        font-size: 16px;
        padding: 10px;
    }
</style>
<?php include("header.php")?>
<!-- x-editor CSS
		============================================ -->
    <link rel="stylesheet" href="css/editor/select2.css">
    <link rel="stylesheet" href="css/editor/datetimepicker.css">
    <link rel="stylesheet" href="css/editor/bootstrap-editable.css">
    <link rel="stylesheet" href="css/editor/x-editor-style.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="css/data-table/bootstrap-table.css">
    <link rel="stylesheet" href="css/data-table/bootstrap-editable.css">
<body>
    <?php include("sidebar.php")?>
    <div class="all-content-wrapper">
        <div class="header-advance-area">
            <div class="header-top-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header-top-wraper">
                                <div class="row">
                                    <div class="col-lg-1 col-md-0 col-sm-1 col-xs-12">
                                        <div class="menu-switcher-pro">
                                            <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
													<i class="educate-icon educate-nav"></i>
												</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div></br></br></br> 
            <!-- Mobile Menu start -->
            <?php include("menubar.php")?>
            <!-- Mobile Menu end -->
            <div class="breadcome-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="breadcome-list single-page-breadcome">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="breadcome-heading">
                                            <form role="search" class="sr-input-func">
                                                <input type="text" placeholder="Search..." class="search-int form-control">
                                                <a href="#"><i class="fa fa-search"></i></a>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <ul class="breadcome-menu">
                                            <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                            </li>
                                            <li><span class="bread-blod">Bank Details</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Static Table Start -->
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <form method='post' id='bankform'>
                            <div class="sparkline13-list">
                                <div class="sparkline13-graph">
                                    <div class="datatable-dashv1-list custom-datatable-overright">
                                        <div class="form-group">
                                            <select class="drop" name="agent">
                                                <?php 
                                                if($stmt->affected_rows > 0  ){
                                                while($stmt->fetch()){
                                                ?>
                                                    <option value="<?php echo $id; ?>"><?php echo $agent_name;?></option>
                                                <?php }
                                                }else{
                                                    echo "No Data Found!";
                                                }?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Account No" name="account" pattern="[0-9]{,18}" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="IFSC" name="ifsc" required>
                                        </div>
                                        <input type="submit" class="btn btn-success waves-effect" value="Submit" name="Submit">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Static Table End -->
        <?php include("footer.php")?>
    </div>
    <?php include("footerjs.php")?>
  
   
   
    <!-- data table JS
		============================================ -->
    <script src="js/data-table/bootstrap-table.js"></script>
    <script src="js/data-table/tableExport.js"></script>
    <script src="js/data-table/data-table-active.js"></script>
    <script src="js/data-table/bootstrap-table-editable.js"></script>
    <script src="js/data-table/bootstrap-editable.js"></script>
    <script src="js/data-table/bootstrap-table-resizable.js"></script>
    <script src="js/data-table/colResizable-1.5.source.js"></script>
    <script src="js/data-table/bootstrap-table-export.js"></script>
    <!--  editable JS
		============================================ -->
    <script src="js/editable/jquery.mockjax.js"></script>
    <script src="js/editable/mock-active.js"></script>
    <script src="js/editable/select2.js"></script>
    <script src="js/editable/moment.min.js"></script>
    <script src="js/editable/bootstrap-datetimepicker.js"></script>
    <script src="js/editable/bootstrap-editable.js"></script>
    <script src="js/editable/xediable-active.js"></script>
    
   
</body>

</html>