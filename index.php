<?php 

  include("conn.php");
   session_start();
  if($_SERVER["REQUEST_METHOD"] == "POST") {
  $username = mysqli_real_escape_string($conn,$_POST['username']);
  $password = mysqli_real_escape_string($conn,$_POST['password']); 
  // echo $username;
  $stmt = $conn->prepare("SELECT id FROM `user` WHERE username = ? AND password = ? ");
    $stmt->bind_param('ss',$username,$password);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($id);
    $stmt->fetch();
    $count =$stmt->affected_rows;
    if($count == 1) {
      // session_register("username");
      $_SESSION['login_user'] = $username;
      
      // header("location: analytics.php");
      header("location: all-provider.php");
   }else {
      $error = "Your Login Name or Password is invalid";
   }
   }

?>
<!doctype html>
<html class="no-js" lang="en">

<?php include("header.php");?>
    <!-- forms CSS
		============================================ -->
    <link rel="stylesheet" href="css/form/all-type-forms.css">
<body>

	<div class="error-pagewrap">
		<div class="error-page-int">
			<div class="text-center m-b-md custom-login">
				<h3>LOGIN </h3>
			</div>
			<div class="content-error">
				  <div class="hpanel">
              <div class="panel-body">
                  <form action="" id="loginForm" method="POST">
                      <div class="form-group">
                          <label class="control-label" for="username">Username</label>
                          <input type="text" placeholder="example@gmail.com" title="Please enter you username" required="" value="" name="username" id="username" class="form-control">
                      </div>
                      <div class="form-group">
                          <label class="control-label" for="password">Password</label>
                          <input type="password" title="Please enter your password" placeholder="******" required="" value="" name="password" id="password" class="form-control">
                      </div>
                      <div class="checkbox login-checkbox">
                          <label>
                      </div>
                      <!-- <input  class="btn btn-success btn-block loginbtn" onClick=" login()">Login</button> -->
                      <input type = "submit" value = " Login " class="btn btn-success btn-block loginbtn"/>
                  </form>
              </div>
          </div>
			</div>
			<div class="text-center login-footer">
				<p>Copyright © 2019. All rights reserved by Vorkup</p>
			</div>
		</div>   
    </div>
    <?php include("footerjs.php")?>
    <!-- icheck JS
		============================================ -->
    <script src="js/icheck/icheck.min.js"></script>
    <script src="js/icheck/icheck-active.js"></script>
</body>

</html>