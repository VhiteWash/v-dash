<?php
    include("conn.php");
    $stmt = $conn->prepare("SELECT oc.id,oc.order_id,c.name,sc.name,s.service,a.name,oc.price,cus.name,o.name,oc.status,oc.created_at FROM order_cart oc JOIN category c ON oc.category_id = c.id JOIN subcategory sc ON oc.subcategory_id = sc.id JOIN services s ON oc.service_id = s.id JOIN agent a ON oc.agent_id = a.id JOIN customer cus ON oc.customer_id = cus.id JOIN `order` o ON oc.order_id = o.id ORDER BY oc.id DESC");
    // echo "SELECT oc.id,oc.order_id,c.name,sc.name,s.service,a.name,oc.price,cus.name,o.name,oc.status,oc.created_at FROM order_cart oc JOIN category c ON oc.category_id = c.id JOIN subcategory sc ON oc.subcategory_id = sc.id JOIN services s ON oc.service_id = s.id JOIN agent a ON oc.agent_id = a.id JOIN customer cus ON oc.customer_id = cus.id JOIN order o ON oc.order_id = o.id ORDER BY oc.id DESC";
    // exit;
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($id,$order_id,$cat_name,$subcat_name,$s_name,$a_name,$price,$cus_name,$address,$status,$date);
?>
<!doctype html>
<html class="no-js" lang="en">

<?php include("header.php")?>
<!-- x-editor CSS
		============================================ -->
    <link rel="stylesheet" href="css/editor/select2.css">
    <link rel="stylesheet" href="css/editor/datetimepicker.css">
    <link rel="stylesheet" href="css/editor/bootstrap-editable.css">
    <link rel="stylesheet" href="css/editor/x-editor-style.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="css/data-table/bootstrap-table.css">
    <link rel="stylesheet" href="css/data-table/bootstrap-editable.css">
<body>
    <?php include("sidebar.php")?>
    <div class="all-content-wrapper">
        <div class="header-advance-area">
            <div class="header-top-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header-top-wraper">
                                <div class="row">
                                    <div class="col-lg-1 col-md-0 col-sm-1 col-xs-12">
                                        <div class="menu-switcher-pro">
                                            <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
													<i class="educate-icon educate-nav"></i>
												</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div></br></br></br> 
            <!-- Mobile Menu start -->
            <?php include("menubar.php")?>
            <!-- Mobile Menu end -->
            <div class="breadcome-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="breadcome-list single-page-breadcome">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="breadcome-heading">
                                            <form role="search" class="sr-input-func">
                                                <input type="text" placeholder="Search..." class="search-int form-control">
                                                <a href="#"><i class="fa fa-search"></i></a>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <ul class="breadcome-menu">
                                            <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                            </li>
                                            <li><span class="bread-blod">Order Table</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Static Table Start -->
        <div class="data-table-area mg-b-15">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="sparkline13-list">
                            <!-- <div class="sparkline13-hd">
                                <div class="main-sparkline13-hd">
                                    <h1>Projects <span class="table-project-n">Data</span> Table</h1>
                                </div>
                            </div> -->
                            <div class="sparkline13-graph">
                                <div class="datatable-dashv1-list custom-datatable-overright">
                                    <!-- <div id="toolbar">
                                        <select class="form-control dt-tb">
											<option value="">Export Basic</option>
											<option value="all">Export All</option>
											<option value="selected">Export Selected</option>
										</select>
                                    </div> -->
                                    <!-- <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                        data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar"> -->
                                    <table id = "table" data-toggle="table">
                                        <thead>
                                            <tr>
                                                <!-- <th data-field="state" data-checkbox="true"></th> -->
                                                <th data-field="id">Order ID</th>
                                                <th data-field="name" data-editable="true">Category Name</th>
                                                <th data-field="name" data-editable="true">SubCategory Name</th>
                                                <th data-field="name" data-editable="true">Service Name</th>
                                                <th data-field="name" data-editable="true">Agent Name</th>
                                                <th data-field="name" data-editable="true">Customer Name</th>
                                                <th data-field="name" data-editable="true">Customer Name</th>
                                                <th data-field="price" data-editable="true">Price</th>
                                                <!-- <th data-field="phone" data-editable="true">Phone</th> -->
                                                <th data-field="complete">Status</th>
                                                <!-- <th data-field="task" data-editable="true">Task</th> -->
                                                <th data-field="date" data-editable="true">Date</th>
                                                <!-- <th data-field="price" data-editable="true">Price</th> -->
                                                <!-- <th data-field="action">Action</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                            <?php
                                            while($stmt->fetch())
                                            {
                                            ?>
                                                <!-- <td></td> -->
                                                <td><?php echo $order_id; ?></td>
                                                <td><?php echo $cat_name; ?></td>
                                                <td><?php echo $subcat_name; ?></td>
                                                <td><?php echo $s_name; ?></td>
                                                <td><?php echo $a_name; ?></td>
                                                <td><?php echo $price; ?></td>
                                                <td><?php echo $cus_name; ?></td>
                                                <td><?php echo $address; ?></td>
                                                <td><?php echo $status; ?></td>
                                                <td><?php echo $date; ?></td>

                                            <?php } ?>
                                                <!-- <td class="datatable-ct"><i class="fa fa-check"></i> -->
                                                <!-- </td>
                                                <td>1</td>
                                                <td>Web Development</td>
                                                <td>admin@uttara.com</td>
                                                <td>+8801962067309</td>
                                                <td class="datatable-ct"><span class="pie">1/6</span>
                                                </td>
                                                <td>10%</td>
                                                <td>Jul 14, 2017</td>
                                                <td>$5455</td>
                                                <td class="datatable-ct"><i class="fa fa-check"></i>
                                                </td> -->
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Static Table End -->
        <?php include("footer.php")?>
    </div>
    <?php include("footerjs.php")?>
  
   
   
    <!-- data table JS
		============================================ -->
    <script src="js/data-table/bootstrap-table.js"></script>
    <script src="js/data-table/tableExport.js"></script>
    <script src="js/data-table/data-table-active.js"></script>
    <script src="js/data-table/bootstrap-table-editable.js"></script>
    <script src="js/data-table/bootstrap-editable.js"></script>
    <script src="js/data-table/bootstrap-table-resizable.js"></script>
    <script src="js/data-table/colResizable-1.5.source.js"></script>
    <script src="js/data-table/bootstrap-table-export.js"></script>
    <!--  editable JS
		============================================ -->
    <script src="js/editable/jquery.mockjax.js"></script>
    <script src="js/editable/mock-active.js"></script>
    <script src="js/editable/select2.js"></script>
    <script src="js/editable/moment.min.js"></script>
    <script src="js/editable/bootstrap-datetimepicker.js"></script>
    <script src="js/editable/bootstrap-editable.js"></script>
    <script src="js/editable/xediable-active.js"></script>
    
   
</body>

</html>